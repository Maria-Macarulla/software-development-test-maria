function ecg = normalising(varargin)
%normalising sets the mean value towards zero and divides by the highest
%ecg value in all channels.
%Inputs: ECG data
%Output: ECG data normalised
switch nargin
    case 0
        fprintf('Error: missing ECG data.\n');
        return;
    case 1
        ecg=varargin{1};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end

N=size(ecg,1); %number of samples
ecg=ecg-repmat(mean(ecg),N,1);
ecg=ecg/max(max(abs(ecg)));
fprintf('ECG data normalised.\n');
end

