function ecg = checkConnection(varargin)
% checkConnection detects if a given channel is not connected and removes it
% input: ecg data, tolerance (optional)
% output: ecg data without disconnected channels

switch nargin
    case 0
        fprintf('Error: missing ECG data.\n');
        return;
    case 1
        ecg = varargin{1};
        tol=eps; %if there is no tolerance, it is set to epsilon machine
    case 2
        ecg=varargin{1};
        tol=varargin{2};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end

mask=mean(abs(ecg))<tol;
if all(mask(:)==0)
    fprintf('All channels connected.\n');
else
    empty_channels=find(mask);
    fprintf('Warning: Channel %d is not connected.\n',empty_channels); %prints which channels are not connected
    ecg(:,empty_channels)=[];%removes empty channels
    fprintf('Channel %d removed.\n',empty_channels);
end
end

