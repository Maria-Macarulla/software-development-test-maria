function ecg = powerlineInterferenceFiltering(varargin)
%powerlineInterferenceFiltering eliminates the noise caused by the powerline at 50Hz
%Stop band finite impulse response filter (fir filter to avoid phase
%distortion)
%Inputs: ECG data, sampling frequency [Hz],both cut-off freqs [Hz],
% order of filter (optional)
%Output: ecg with no powerline interference
switch nargin
    case 0
        fprintf('Error: missing arguments.\n');
        return;
    case 1
        fprintf('Error: missing arguments.\n');
        return;
    case 2
        fprintf('Error: missing arguments.\n');
        return;
    case 3
        fprintf('Error: missing arguments.\n');
        return;
    case 4
        ecg=varargin{1};
        fs=varargin{2};
        fc_l = varargin{3};
        fc_h = varargin{4};
        n=150;%order higher than 150 presents ripples in the stop band
    case 5
        ecg=varargin{1};
        fs=varargin{2};
        fc_l = varargin{3};
        fc_h = varargin{4};
        n=varargin{5};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end

fn = fs/2; %nyquist freq
b=fir1(n,[fc_l,fc_h]/fn,'stop');
% freqz(b,1)%to see filter plot
ecg=filter(b,1,ecg);
fprintf('Powerline interference at %d Hz filtered.\n',(fc_l+fc_h)/2*fn)
end

