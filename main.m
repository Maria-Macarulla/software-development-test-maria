clear;close all;clc;
%% ECG signal processing
load('ecgConditioningExample');

%Check if channels are connected
ecg_connected = checkConnection(ecg);

%Recording spikes removal
ecg_noSpike = recordingSpikesRemoval(ecg_connected);

%Normalise data
ecg_norm=normalising(ecg_noSpike);

%Low-pass filtering
ecg_lpf = lowPassFiltering(ecg_norm,fs,160,10);

%Powerline interference filtering
ecg_nopower = powerlineInterferenceFiltering(ecg_lpf,fs,40,60,150);% @50Hz
ecg_nopower = powerlineInterferenceFiltering(ecg_nopower,fs,90,110,150);% @100Hz, first harmonic
ecg_nopower = powerlineInterferenceFiltering(ecg_nopower,fs,140,160,150);% @150Hz, second harmonic


%Baseline wander and offset removal.
ecg_nobase=baselinewanderAndOffsetRemoval(ecg_nopower,fs,0.67,1500);


%Moving average filter
ecg_avg = movingAverageFilter(ecg_nobase,20);


%% Plots
[N,ch]=size(ecg_connected); %number of samples and channels connected
L=2^nextpow2(N);
f=fs*(0:L-1)/L;%frequency axis
t=(0:N-1)/fs;%time axis

for cc=1:ch
    figure(cc);
    sgtitle(['ECG data from channel',' ',num2str(cc)]);
    
    subplot(5,2,1);plot(t,ecg_norm(:,cc));title('Spikes removed and normalised');xlabel('t [s]')
    subplot(5,2,2);plot(f,abs(fft(ecg_norm(:,cc),L)/L));xlim([0 0.5*fs]);title('Spikes removed and normalised');xlabel('f [Hz]');
    
    subplot(5,2,3);plot(t,ecg_lpf(:,cc));title('Low-pass filtered');xlabel('t [s]')
    subplot(5,2,4);plot(f,abs(fft(ecg_lpf(:,cc),L)/L));xlim([0 0.5*fs]);title('Low-pass filtered');xlabel('f [Hz]');
    
    subplot(5,2,5);plot(t,ecg_nopower(:,cc));title('Powerline noise removed');xlabel('t [s]')
    subplot(5,2,6);plot(f,abs(fft(ecg_nopower(:,cc),L)/L));xlim([0 0.5*fs]);title('Powerline noise removed');xlabel('f [Hz]');
    
    subplot(5,2,7);plot(t,ecg_nobase(:,cc));title('Baseline wander and offset removed');xlabel('t [s]')
    subplot(5,2,8);plot(f,abs(fft(ecg_nobase(:,cc),L)/L));xlim([0 0.5*fs]);title('Baseline wander and offset removed');xlabel('f [Hz]');
    
    subplot(5,2,9);plot(t,ecg_avg(:,cc));title('Moving average filtered');xlabel('t [s]')
    subplot(5,2,10);plot(f,abs(fft(ecg_avg(:,cc),L)/L));xlim([0 0.5*fs]);title('Moving average filtered');xlabel('f [Hz]');
end