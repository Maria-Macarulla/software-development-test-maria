# Software Development Test Maria

## Description
Electrocardiogram signal pre-processing test

## Files

- _ecgConditioningExample.mat_: data for test.
- _main.m_: loads ECG data and calls functions to process the ECG data.
- _checkConnection.m_: detects whether a given channel is connected. The not connected channels are removed.
- _recordingSpikesRemoval.m_: eliminates the artifacts of great amplitude.
- _normalising.m_: normalises the given signal.
- _lowPassFiltering.m_: eliminates high frequency noise.
- _powerlineInterferenceFiltering.m_: eliminates the noise caused by the powerline.
- _baselinewanderAndOffsetRemoval.m_: removes offset and noise caused by movement, breathing, etc.
- _movingAverageFilter.m_: applies a moving average filter in order to smooth the signal.

## Results 

The results obtained of each channel through different steps of the pre-processing are shown below.

<img src="results/results_ch1.jpg" width="430" >
<img src="results/results_ch2.jpg" width="430" >
<img src="results/results_ch3.jpg" width="430" >
<img src="results/results_ch4.jpg" width="430" >
<img src="results/results_ch5.jpg" width="430" >

## Authors and acknowledgment
Maria Macarulla.
e-mail: macarulla.maria@gmail.com

## Project status
Finished.
