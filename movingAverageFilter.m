function ecg = movingAverageFilter(varargin)
%movingAverageFilter applies a moving average filter in order to smooth
%the signal.
%Inputs: ECG data, number of coefficients (optional)
%Output: smooth ECG

switch nargin
    case 0
        fprintf('Error: missing ECG data.\n');
        return;
    case 1
        ecg=varargin{1};
        c=20; %the higher the coefficients the smoother
    case 2
        ecg=varargin{1};
        c=varargin{2};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end

b=1/c*ones(c,1);
ecg = filter(b,1,ecg);
fprintf('Moving Average filter applied.\n');
end

