function ecg = baselinewanderAndOffsetRemoval(varargin)
%baselinewanderAndOffsetRemoval removes offset and noise caused by
%movement, breathing, etc.
%FIR filter presents linear phase and avoids ST-segment distortion 
%HPF with cut-off freq of 0.67Hz (corresponds to 40bpm)
%Inputs: ECG data, sampling frequency [Hz],cut-off freq (optional) [Hz],
% order of filter (optional)
%Output: ecg high pass filtered
switch nargin
    case 0
        fprintf('Error: missing ECG and fs data.\n');
        return;
    case 1
        fprintf('Error: missing ECG or fs data.\n');
        return;
    case 2
        ecg=varargin{1};
        fs=varargin{2};
        fc = 0.67;%40 bpm
        n=1000;%high order to attenuate more
    case 3
        ecg=varargin{1};
        fs=varargin{2};
        fc = varargin{3};
        n=1000;
    case 4
        ecg=varargin{1};
        fs=varargin{2};
        fc = varargin{3};
        n=varargin{4};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end

fn = fs/2; %nyquist freq
b=fir1(n,fc/fn,'high');
% freqz(b,1)%to see filter plot
ecg=filter(b,1,ecg);
fprintf('ECG data high-pass filtered.\n')

end

