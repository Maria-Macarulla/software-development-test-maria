function ecg = lowPassFiltering(varargin)
%lowPassFiltering eliminates EMG noise (above 100Hz)
%Butterworth iir filter is chosen because it has fewer ripples in the stop-band and pass-band
%Inputs: ECG data, sampling frequency [Hz],cut-off freq (optional) [Hz],
% order of filter (optional)
%Output: ecg low pass filtered
switch nargin
    case 0
        fprintf('Error: missing ECG and fs data.\n');
        return;
    case 1
        fprintf('Error: missing ECG or fs data.\n');
        return;
    case 2
        ecg=varargin{1};
        fs=varargin{2};
        fc = 160;%above 160 to make sure that signal of patient is not removed
        n=10;
    case 3
        ecg=varargin{1};
        fs=varargin{2};
        fc = varargin{3};
        n=10;
    case 4
        ecg=varargin{1};
        fs=varargin{2};
        fc = varargin{3};
        n=varargin{4};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end

fn = fs/2; %nyquist freq
[b,a] = butter(n,fc/fn);
% freqz(b,a) %to see filter plot
ecg = filter(b,a,ecg);
fprintf('ECG data low-pass filtered.\n')
end

