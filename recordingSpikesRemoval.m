function ecg = recordingSpikesRemoval(varargin)
%recordingSpikesRemoval eliminates the artifacts of great amplitude
%input: ecg, tolerance (optional)
%output: ecg without recording spikes

switch nargin
    case 0
        fprintf('Error: missing ECG data.\n');
        return;
    case 1
        ecg = varargin{1};
        tol=1e4; %if there is no tolerance, it is set higher than the standard deviation of signal without peaks (~4k)
    case 2
        ecg=varargin{1};
        tol=varargin{2};
    otherwise
        fprintf('Error: too many arguments.\n');
        return;
end
%number of samples and available channels
[N,ch]=size(ecg);

%ECG data sorted, highest values first
ecg_sorted = sort(ecg,'descend');

%Calculate the value of the peaks that are tol times higher than the maximum of ecg data.
%I add a row of logical zeros because diff returns a matrix with one row less.
peak = ecg_sorted([diff(ecg_sorted)<-tol; logical(zeros(1,ch))]);

if(~isempty(peak))
    
    %create the mask of peaks
    peak_mask=ecg>=min(peak);
    
    %set the peaks to zero
    ecg(peak_mask)=0;
    
    %set the empty places to the mean value of each channel
    ecg=ecg+peak_mask.*repmat(mean(ecg),N,1);
    fprintf('Spikes removed.\n')
else
    fprintf('No spikes found.\n')
end
end

